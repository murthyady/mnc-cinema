import Axios from "axios";

const token =
  "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4ZTg0YjY4OGExNTZhNzk0NjczMzlmYzE2MjkyYjcwZSIsInN1YiI6IjYyZWM3ZTZhZWE4NGM3MDA2MTNhZmM1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.gM8GkptuOXkV61Ti6Kn8nm4xIxMCnA6UOcU1VAU9eTU";

const apiClient = Axios.create({
  baseURL: "https://api.themoviedb.org/3",
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export default {
  getAllMoviePopular() {
    return apiClient.get("/movie/popular");
  },
  getDetailMoviePopular(id) {
    return apiClient.get(
      `/movie/${id}?append_to_response=credits,videos,images`
    );
  },
  getSearchMovie(search) {
    return apiClient.get(`/search/movie?query=${search}`);
  },
};
