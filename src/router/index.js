import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@/views/HomePage";
import DetailFilmPage from "@/views/DetailFilmPage";
import SearchPage from "@/views/SearchPage";

const routes = [
  {
    path: "/",
    name: "homepage",
    component: HomePage,
  },
  {
    path: "/film/:id",
    name: "detailfilmpage",
    component: DetailFilmPage,
    props: true,
  },
  {
    path: "/search/:search",
    name: "searchpage",
    component: SearchPage,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
